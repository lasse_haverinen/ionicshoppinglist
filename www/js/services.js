angular.module('starter.services', [])

.factory('ShoppingListData', function() {
  // Might use a resource here that returns a JSON array

    var items = JSON.parse(localStorage.getItem('savedListData'));
    if(items == null)
    {
        items = [];
    }
    
    var activeList = [];
  
    // Some fake testing data
    /*var items = [{ amount: 2,
                 type: "Ketsuppia"},
               { amount: 5,
                 type: "Banaaneja"},
               { amount: 1,
                 type: "Maitoa"},
               { amount: 500,
                 type: "Kanaa"},
    ];*/

  return {
        getActiveListData: function() {
            return activeList;
        },
        
        all: function() {
            return items;
        },
        save: function(data) {
            items.push(data);
            localStorage.setItem('savedListData', JSON.stringify(items));
        },
        activate:  function(listData)
        {
            activeList = listData;
            
        },
        clearActive: function()
        {
            activeList = [];
        },
        remove: function()
        {
            
        }
  }
});





