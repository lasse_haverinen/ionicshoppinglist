angular.module('starter.controllers', [])

.controller('ActiveCtrl', function($scope, $ionicPopup, ShoppingListData) {
    $scope.listData = ShoppingListData.getActiveListData();
    $scope.newItemInput = { value : ""};
    
    $scope.listName = {};
    $scope.isFocused = true;
    
    $scope.addItem = function()
    {        
        var type = $scope.newItemInput.value;
        if (type.length == 0)
        {
            return;
        }
        var found = false;
        for(var i = 0; i < $scope.listData.length; i++)
        {
            if($scope.listData[i].type == type)
            {
                $scope.listData[i].amount++;
                found = true;
                break;
            }
        }
        
        if(found == false)
        {
            $scope.listData.push({ amount: 1, type: type }); 
        }
        $scope.newItemInput.value = "";
    };
    
    $scope.change = function()
    {
        console.log($scope.newItemInput.value);
    }
    
    $scope.increaseAmount = function(item, $event)
    {
        item.amount++;
        if ($event.stopPropagation) $event.stopPropagation();
        if ($event.preventDefault) $event.preventDefault();
    }
    
    $scope.decreaseAmount = function(item, $event)
    {
        item.amount--;
        if ($event.stopPropagation) $event.stopPropagation();
        if ($event.preventDefault) $event.preventDefault();
    }
    
    $scope.empty = function()
    {
        
            var confirmPopup = $ionicPopup.confirm({
                title: 'Clear list',
                template: 'Are you sure you want to clear this list?'
            });
            confirmPopup.then(function(res) {
                if(res) 
                {
                    $scope.listData = [];
                    ShoppingListData.clearActive();
                } 
                else
                {
                //    console.log('You are not sure');
                }
            });
        
    
        
    }
    
    $scope.save = function() {
        
        var myPopup = $ionicPopup.show({
            template: '<input type="text" ng-model="listName.value">',
            title: "Saving list",
            subTitle: "Please enter list name",
            scope: $scope,
            buttons: [
            { text: 'Cancel' },
            { text: 'Save',
              type: 'button-positive',
              onTap: function(e) {
                return $scope.listName.value;
              }
            }
            ]
        });
        
        myPopup.then(function (res) {
            var saveItem = {listName: res,
                            listData: angular.copy($scope.listData)};
            ShoppingListData.save(saveItem);
            console.log("saving list with name: " + res);
        });
        
    };
    
})

.controller('SavedCtrl', function($scope, ShoppingListData) {
    $scope.lists = ShoppingListData.all();
    
    $scope.activate = function(item)
    {
        ShoppingListData.activate(item.listData);
    }
})

.controller('FriendDetailCtrl', function($scope, $stateParams, Friends) {
  $scope.friend = Friends.get($stateParams.friendId);
})


